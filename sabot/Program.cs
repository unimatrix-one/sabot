﻿//#define DBG_READ_POS
//#define DBG_WRITE_POS
//#define SHOW_2ND
#define AVE
#define sabot
using System;
using System.Buffers;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using shift;

namespace sabot
{
    class Program
    {
        private const float N = 1;
        private const float X = N;
        private static readonly float Y = (float)-(-Math.Pow(N, 2) - 1);
        private static readonly float[] S = { 1, -X, 0, 0, X, 1, -Y, 0, 0, -Y, 1, X, 0, 0, -X, 1 };

        private const int Rounds = 3;
        
        private static int cTrivial;
        private static int cTrivialConverge = 0;
        private static bool diverged = false;

        static long zeroDetAve = 0;
        private static long swapLeft;
        private static long swapRight;
        //private static Vector<float> meanDiag = Vector<float>.Build.Dense(4, 0);

        static HashEqualityComparer hashComparer= new HashEqualityComparer();

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        static (long, int) SqueezeShaCompactHash(byte[] buffer)
        {
            var hash = sha.ComputeHash(buffer);
            return (MemoryMarshal.Cast<byte, long>(hash.AsSpan().Slice(0, 8))[0], hash.Length);
        }

        [ThreadStatic]
        private static SHA512 sha = SHA512.Create();

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        static byte[] SqueezeShaHash(byte[] buffer)
        {
            sha ??= SHA512.Create();

            return sha.ComputeHash(buffer);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        static (TBits, int, int) SqueezeShortHash<TBits>(Span<byte> buffer, bool printDetStats = false)
        where TBits : struct
        {
            var hash = Sabot.Squeeze(buffer, Rounds).Slice(0, Marshal.SizeOf(default(TBits)));
            return (MemoryMarshal.Cast<byte, TBits>(hash)[0], hash.Length, Rounds);
        }

        /// <summary>
        /// Squeeze
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="r">Rounds performed</param>
        /// <param name="offset"></param>
        /// <returns>Hashes</returns>
        

        static void Collide<TBits>()
        //where TBits : struct
        //where TBits : byte[]
        {
            var stopWatch = Stopwatch.StartNew();
            long masterCount = 0;
            long count = 1;
            long aveCount = 1;
            long collisionCount = 0;
            long preImageCount = 0;
            long preImageMatch = 0;
            long dupCount = 0;
            long aveByte = 0;
            long minByte = 0;
            long maxByte = 0;
            long uintsHashed = 1;
            long charsHashed = 1;
            long compressionRatio = 0;
            long aveInputSize = 0;
            long aveConvertTicks = 0;
            long aveRounds = Rounds;
            long dupCheckTicks = 0;
            long setStoreTicks = 0;
            long aveDictionaryTickTime;
            long aveDictionaryTicks = 0;
            long iterations = 5000000000;
            long powHits = 1;

            //ConcurrentDictionary<TBits, byte[]> collisions = new ConcurrentDictionary<TBits, byte[]>(16, (int)(runs*1.1));
            var collisions = new ConcurrentDictionary<byte[], byte[]>(ThreadCount, (int)(iterations), hashComparer);

            var collisions_h = new Hashtable();
            //var collisions = new ConcurrentDictionary<byte[], byte[]>();
            var sentinel = new byte[8];

            Console.WriteLine($"Memory = {(float)GC.GetTotalMemory(true) / 1024 / 1024:0.0}Mb");

            var collider = new Action<int>((i) =>
            {
                var j = i*2;
                while (true)
                {
                    var offset = 0;
                    var index1Str = $"{j}";
                    var index2Str = $"{j+1}";
                    var imageBase = SqueezeShaHash(Encoding.ASCII.GetBytes(index1Str)).Concat(SqueezeShaHash(Encoding.ASCII.GetBytes(index2Str))).ToArray().AsSpan();

                    while (offset < 64)
                    {
                        //var imageMeta = Encoding.ASCII.GetBytes($"{i},{j},{offset}|");
                        //var image = imageMeta.Concat(imageBase).ToArray().AsSpan();
                        var image = imageBase;
                        //var metaOffset = Array.FindIndex(imageMeta, b => b==(byte)'|');
                        //var imageFinal = image.Slice(offset + metaOffset, 64);
                        var imageFinal = image.Slice(offset, 64);
                        //var imageFinal = imageBase;

                        var stopwatch = Stopwatch.StartNew();
#if sabot
                        var hash = Sabot.Squeeze(imageFinal, Rounds, 0, 64).ToArray();
#else
                        var hash = SqueezeShaHash(strBuffer);
#endif
                        stopwatch.Stop();

                        Interlocked.Add(ref aveConvertTicks, stopwatch.ElapsedTicks);

                        
                        var key = hash.AsSpan().Slice(0, 8).ToArray();

                        stopwatch.Restart();
                        var containsKey = collisions.ContainsKey(key);
                        stopwatch.Stop();
                        Interlocked.Add(ref dupCheckTicks, stopwatch.ElapsedTicks);
                        try
                        {
#if SHOW_2ND
                            if (containsKey)
                            {
                                var chashOffset = Array.FindIndex(collisions[key], c => c == (byte)'|') + 1;
                                var imageCollision = collisions[key].AsSpan().Slice(chashOffset, 64);

                                if (!(hashComparer.Equals(imageCollision, imageFinal)))
                                {
                                    var hashB64 = Convert.ToBase64String(hash);
                                    var collision = collisions[key].AsSpan().Slice(chashOffset, 64).ToArray();
                                    var chash = Sabot.Squeeze(collision, Rounds, i, 0, 64).ToArray();
                                    var chashB64 = Convert.ToBase64String(chash);

                                    if (hashB64 == chashB64)
                                    {
                                        Console.WriteLine($"COLLISION!!! \nstr1=`{Convert.ToBase64String(imageFinal)}'\n\nstr2=`{Convert.ToBase64String(collision)}'\n\n[{hashB64}]\ncount = `{collisions.Count}'\n");
                                    }
                                    Interlocked.Increment(ref collisionCount);
                                }
                            }
                            else
#endif
                            {
#if SHOW_2ND
                                //var compression = await str.Zip();
                                var compression = imageMeta.Concat(imageFinal.ToArray()).ToArray();
;
#else
                                 var compression = sentinel;
#endif

                                
                                stopwatch.Restart();
                                if (collisions.TryAdd(key, compression))
                                {
                                    stopwatch.Stop();

                                    Interlocked.Add(ref compressionRatio, (image.Length - compression.Length) * 100 / image.Length);
#if AVE
                                    int min = int.MaxValue, max = int.MinValue;
                                    var countableBytes = hash.ToArray().Select(b =>
                                    {
                                        int intb = b;

                                        if (b < min)
                                            min = b;
                                        if (b > max)
                                            max = b;

                                        return intb;
                                    }).ToList();


                                    Interlocked.Add(ref aveByte, (long)countableBytes.Average());
                                    Interlocked.Add(ref minByte, min);
                                    Interlocked.Add(ref maxByte, max);
#endif
                                    Interlocked.Add(ref uintsHashed, key.Length);
                                    Interlocked.Add(ref charsHashed, 64);
                                    Interlocked.Add(ref aveInputSize, image.Length);
                                    Interlocked.Add(ref aveRounds, Rounds);
                                    Interlocked.Increment(ref aveCount);
                                }
                                stopwatch.Stop();
                                Interlocked.Add(ref setStoreTicks, stopwatch.ElapsedTicks);
                            }

                            if (hash[0] == 0 && hash[1] == 0 )
                            {
                                //Console.WriteLine(Convert.ToBase64String(hash));
                                Interlocked.Increment(ref powHits);
                            }
                            
                        }
                        catch
                        {
                            // ignored
                        }

                        Interlocked.Increment(ref count);
                        Interlocked.Increment(ref masterCount);
#if DEBUG
                        var logF = 100000;
                        aveDictionaryTickTime = logF * 2;
#else
                        var logF = 1000000;
               aveDictionaryTickTime = logF * 2;
#endif
                        if ((count % logF) == 0)
                        {
                            var collisionsRecorded = collisions.Count;
                            Console.WriteLine($"Count = {collisionCount}/{count}({masterCount})<{preImageCount}>, ave = {(double)aveByte / aveCount:0.000} [{minByte / aveCount}/{maxByte / aveCount}], pow = {masterCount / (powHits) :0}K, in = {(charsHashed / 1024.0) / stopWatch.Elapsed.TotalSeconds:0} Kb/s, size = {aveInputSize / aveCount}, out = {(uintsHashed / 1024.0) / stopWatch.Elapsed.TotalSeconds:0} Kb/s, zip = {100.0 - (float)compressionRatio / aveCount:0.0}%, {count / stopWatch.Elapsed.TotalSeconds:0000.0} c/s, ({dupCheckTicks / aveDictionaryTicks},{setStoreTicks / aveDictionaryTicks}), table size = {collisionsRecorded}, ratio = {(float)collisionsRecorded / count * 100:0}%, hashTicks = {aveConvertTicks / masterCount}, {((float)aveConvertTicks / masterCount) / (charsHashed / aveCount) * 100:0}ns, r = {aveRounds / aveCount}, Memory = {(float)GC.GetTotalMemory(true) / 1024 / 1024:0.0}Mb");
                            if (collisionsRecorded >= iterations)
                            {
                                var totalMemBefore = GC.GetTotalMemory(true);
                                collisions.Clear();
                                //GC.Collect();
                                //GC.WaitForFullGCComplete();
                                count = 0;
                                charsHashed = 0;
                                uintsHashed = 0;
                                aveCount = 0;
                                compressionRatio = 0;
                                aveRounds = Rounds;
                                aveInputSize = 0;
                                aveByte = 0;
                                minByte = 0;
                                maxByte = 0;
                                stopWatch.Restart();
                                Console.WriteLine($"Before = {(float)totalMemBefore / 1024 / 1024:0.0}Mb, After = {(float)GC.GetTotalMemory(true) / 1024 / 1024:0.0}Mb");
                            }
                        }

                        if (aveDictionaryTicks >= aveDictionaryTickTime)
                        {
                            aveDictionaryTicks = 0;
                            dupCheckTicks = 0;
                            setStoreTicks = 0;
                        }

                        Interlocked.Increment(ref aveDictionaryTicks);

                        offset++;
                    }
                    j += ThreadCount*2;

                }
            });

            var threads = new List<Task>();
            for (var i = 0; i < ThreadCount; i++)
            {
                var i1 = i;
                threads.Add(Task.Factory.StartNew(() => collider(i1),TaskCreationOptions.LongRunning));
            }

            Task.WaitAll(threads.ToArray());
        }

        static string ToString(Span<byte> buffer)
        {
            var builder = new StringBuilder();
            foreach (var t in buffer)
            {
                builder.Append(t.ToString("x2"));
            }
            return builder.ToString();
        }

        static double Hamming(string str1, string str2, Func<byte[], int, int, int, byte[]> hash)
        {
            BitArray h1 = new BitArray(hash(Encoding.ASCII.GetBytes(str1), Rounds, 0, str1.Length).ToArray());
            BitArray h2 = new BitArray(hash(Encoding.ASCII.GetBytes(str2), Rounds, 0, str1.Length).ToArray());

            var butterfly = h1.Xor(h2);

            var hamming = 0;
            foreach (bool b in butterfly)
            {
                if (b)
                    hamming++;
            }

            return (double) hamming / (butterfly.Length) * 100;
        }

        static void ApproxMOD()
        {
            var ave = 0;
            var aveCount = 0;
            for (int i = 0; i < byte.MaxValue * 2; i++)
            {
                for (int j = 1; j < byte.MaxValue * 2; j++)
                {
                    int I = i % j;
                    var J = (i & j) - 12;

                    if ((I) != (J))
                    {
                        //Console.WriteLine($"i = {i}, ({I - J}), {I},{J}");
                        ave += I - J;
                        aveCount++;
                    }
                }
            }
            Console.WriteLine($"ave = {(double)ave / aveCount:0.0}");
        }

        private static int ThreadCount = 8;
        static Sabot Sabot = new();
        static void Main(string[] args)
        {
            //ApproxMOD();
            //Console.ReadLine();
            //return;
            var zero = Encoding.ASCII.GetBytes("");
            //var zero = Enumerable.Range(0, 32).Select(_ => (byte)0).ToArray();
            var zeroHash = Sabot.Squeeze(zero, Rounds,0, zero.Length);
            
            
            var str1 = "The quick brown fox jumps over the lazy dog";
            //var str2 = "The quick brown fox jumps over the lazy dog";


            for (int i = 0; i < 8; i++)
            {
                str1 = str1 + str1;
            }

            var strb1 = new StringBuilder(str1);
            var strb2 = new StringBuilder(str1);

            var H1 = 0.0;
            var H2 = 0.0;
            //for (int j = 0; j < str1.Length; j++)
            //{
            //    strb1[j] = Convert.ToChar(char.ToString((char) (strb1[j] + 1)));
            //    H1+= Hamming(strb1.ToString(), strb2.ToString(), (bytes, i, arg3, arg4) => Sabot.Squeeze(bytes, i, 0, arg3, arg4).ToArray());
            //    H2+= Hamming(strb1.ToString(), strb2.ToString(), (bytes, i, arg3, arg4) => SqueezeShaHash(bytes).ToArray());
            //    strb1 = new StringBuilder(str1);
            //}

            Console.WriteLine($"ave = {zeroHash.ToArray().Select(c => (int)c).Average()}, zero hash = {ToString(zeroHash)}, {H1/ str1.Length:0.0}% <-> {H2/ str1.Length:0.0}%");

            Signature();

            Collide<byte[]>();
        }

        private static void Signature()
        {
            var secretMsg = Encoding.ASCII.GetBytes("1secretMsg");
            var zero = Encoding.ASCII.GetBytes("2secretMsg");

            var pk = Sabot.Squeeze(zero, Rounds,  0, zero.Length).ToArray();
            var pk2 = Sabot.Squeeze(secretMsg, Rounds,  0, secretMsg.Length).ToArray();
            var pub = Sabot.Squeeze(pk, Rounds,  0, pk.Length).ToArray();

            Console.WriteLine($"key = {ToString(pk)}");
            Console.WriteLine($"key = {ToString(pk2)}");
            Console.WriteLine($"pub = {ToString(pub)}");


            var digest = pk.Take(pk.Length/2).ToArray();
            var s = Sabot.Squeeze(digest, Rounds,  0, digest.Length).ToArray();

            Console.WriteLine($"s = {ToString(s)}");

            var feed = pub.Concat(secretMsg).ToArray();
            var sv = Sabot.Squeeze(feed, Rounds,  0, feed.Length).ToArray();

            Console.WriteLine($"v = {ToString(sv)}");
        }
    }
}
