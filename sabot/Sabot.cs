﻿using System;
using System.Runtime.CompilerServices;

namespace shift
{
    public struct Sabot
    {
        private const int BlockLength = 4;
        private const int MinRounds = 3;
        private const int PreMix = 2;
        private const int SecurityLevel = 512 / 8;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Shake(Span<byte> c)
        {
            c[0] = (byte)(c[0] - c[1]);
            c[1] = (byte)(c[0] + c[1] - c[2]);
            c[2] = (byte)(     - c[1] + c[2] + c[3]);
            c[3] = (byte)(            - c[2] + c[3]);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Mix(Span<byte> c, Span<byte> t)
        {
            c[0] = t[0] = (byte)(c[0] + t[0] - t[1]);
            c[1] = t[1] = (byte)(c[1] + t[0] + t[1] - t[2]);
            c[2] = t[2] = (byte)(c[2]        - t[1] + t[2] + t[3]);
            c[3] =        (byte)(c[3]               - t[2] + t[3] );
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void Mix(Span<byte> c, byte t0, byte t1, byte t2, byte t3)
        {
            c[0] = t0 = (byte)(c[0] + t0 - t1);
            c[1] = t1 = (byte)(c[1] + t0 + t1 - t2);
            c[2] = t2 = (byte)(c[2]      - t1 + t2 + t3);
            c[3] =      (byte)(c[3]           - t2 + t3);
        }

        public Span<byte> Squeeze(Span<byte> buffer, int rounds = 0, int offset = 0, int length = 0)
        {
            if (buffer == null)
                throw new ArgumentNullException(nameof(buffer));

            if (buffer.IsEmpty)
                buffer = new[] { (byte)0 };

            rounds += MinRounds;

            Span<byte> hash = stackalloc byte[SecurityLevel + BlockLength];
            hash[0] = 47;

            if (offset > 0 || length > 0)
                buffer = buffer.Slice(offset, length > 0 ? length : buffer.Length - offset);

            var h = 0;
            var H = SecurityLevel;
            var vector = 1;
            
            //shuffle
            for (var j = 0; j < PreMix; j++)
            {
                var i = 0;
                while (i < Math.Max(buffer.Length, BlockLength))
                {
                    if (i + BlockLength < buffer.Length)
                    {
                        Mix(hash[h..], buffer[i..]);
                    }
                    else
                    {
                        Mix(hash[h..],
                            (byte)(buffer[ i      % buffer.Length] ^ 89),
                            (byte)(buffer[(i + 1) % buffer.Length] ^ 157),
                            (byte)(buffer[(i + 2) % buffer.Length] ^ 199),
                            (byte)(buffer[(i + 3) % buffer.Length] ^ 241));
                    }
                    i++;
                    if (++h >= SecurityLevel)
                        h = 0;
                }
            }

            //mix
            for (var r = 0; r < rounds; r++)
            {
                while (h != H)
                {
                    Shake(hash[h..]);
                    h += vector;
                }

                vector *= -1;
                (h, H) = (H, h);
            }
            

            return hash[..^BlockLength].ToArray();
        }
    }
}
