﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace shift
{
    public class HashEqualityComparer : EqualityComparer<byte[]>
    {
        public override bool Equals(byte[] x, byte[] y)
        {
            return Equals(x.AsSpan(), y.AsSpan());
        }

        public bool Equals(Span<byte> x, Span<byte> y)
        {
            if (y != null && (x != null && x.Length != y.Length))
                return false;

            for (var i = x.Length - 1 - x.Length % 2; i > 0; i -= 2)
            {
                if (x[i] != y[i])
                    return false;
            }

            for (var i = 0; i < x.Length; i += 2)
            {
                if (x[i] != y[i])
                    return false;
            }

            return true;
        }

        public override int GetHashCode(byte[] hash)
        { 
            return MemoryMarshal.Cast<byte, int>(hash.AsSpan())[0];
        }
    }
}
